
let qtree

function setup() {
	createCanvas(400, 400)

	let boundary = new Rectangle(200, 200, 200, 200)
	qtree = new QuadTree(boundary, 4)
	
}


function draw () {

	if (mouseIsPressed) {
		let p = new Point(mouseX, mouseY)
		qtree.insert(p)
	}

	background(0)
	qtree.show()
}
